$ -> App.init()

App =
  init: ->
    @initFormHandling()

  initFormHandling: ->
    $form = $("form", "main")
    $form.on "submit", this, (e) =>
      e.preventDefault()
      value = $("#input").val()
      @sendRequest({input: value})

  sendRequest: (requestData) ->
    request = $.ajax
      url: "/decode"
      method: "POST"
      data: requestData
      dataType: "json"

    request.done (response) -> App.handleResponse(response)

  handleResponse: (responseData) ->
    $form = $("form", "main")
    $form.after($("<p/>", {"html": "<a href=mailto:#{responseData.result}>#{responseData.result}</a>", "class": "results" }))