(function() {
  var App;

  $(function() {
    return App.init();
  });

  App = {
    init: function() {
      return this.initFormHandling();
    },
    initFormHandling: function() {
      var $form;
      $form = $("form", "main");
      return $form.on("submit", this, (function(_this) {
        return function(e) {
          var value;
          e.preventDefault();
          value = $("#input").val();
          return _this.sendRequest({
            input: value
          });
        };
      })(this));
    },
    sendRequest: function(requestData) {
      var request;
      request = $.ajax({
        url: "/decode",
        method: "POST",
        data: requestData,
        dataType: "json"
      });
      return request.done(function(response) {
        return App.handleResponse(response);
      });
    },
    handleResponse: function(responseData) {
      var $form;
      $form = $("form", "main");
      return $form.after($("<p/>", {
        "html": "<a href=mailto:" + responseData.result + ">" + responseData.result + "</a>",
        "class": "results"
      }));
    }
  };

}).call(this);
