%w(sinatra haml json base64).each do |lib|
  require lib
end

get '/' do
  settings
  haml :index
end

post "/decode" do
  matchPattern = Regexp.new(/\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b/)
  parsedEmail = Base64.decode64(params[:input]).scan(matchPattern)
  
  content_type :json
  {:result => parsedEmail}.to_json 
end